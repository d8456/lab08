from enum import Enum
from typing import Set, Tuple, Mapping, List
import os

class Color(Enum):
    B = "B"
    W = "W"
    E = "E"

    def getOpposite(color):
        return Color.B if color == Color.W else Color.W

    def __str__(self) -> str:
        if self == Color.B:
            return "B"
        if self == Color.W:
            return "W"
        if self == Color.E:
            return "E"

class Coord:
    def __init__(self, row, col):
        self.row = row
        self.col = col
    
    def __eq__(self, __o: object) -> bool:
        if self.row == __o.row and self.col == __o.col:
            return True
        else:
            return False

    def __ne__(self, __o: object) -> bool:
        return not self.__eq__(__o)
    
    def __hash__(self) -> int:
        return int(self.row) * 11 * int(self.col) * 7

    def __str__(self) -> str:
        return str(self.row) + ':' + str(self.col)

    def __repr__(self) -> str:
        return self.__str__()

class String:
    def __init__(self, *args) -> None:
        """1st arg = liberties: Set
        2nd arg = enemies: Set
        3rd arg = colors: Set
        """
        if len(args) == 3 and all(isinstance(arg, Set) for arg in args):
            self.liberties = args[0]
            self.enemies = args[1]
            self.colors = args[2]
        else:
            self.liberties = set()
            self.enemies = set()
            self.colors = set()

    def __str__(self) -> str:
        return ("<<liberties: " + str(self.liberties)
        + "\nenemies: " + str(self.enemies)
        + "\ncolors: " + str(self.colors) + ">>")

    def __repr__(self) -> str:
        return self.__str__()

#SRP
class Board:

    def getBoard(self):
        return self._board

    def getBoardSize(self) -> int:
        return self._boardSize

    def getScoreboard(self) -> Mapping[Color, int]:
        return self._scoreboard
    
    def getLastPlayer(self) -> Color:
        return self._lastPlayer

    def instantiate(brdSize: int, board: List[Color],
     lstPlayer: Color, scoreboard: Mapping[Color, int],
     filename: str):
        retBrd = Board(brdSize)
        retBrd._board = board
        retBrd._lastPlayer = lstPlayer
        retBrd._scoreboard = scoreboard
        return retBrd

    def __init__(self, brdSize):
        self._boardSize = brdSize
        self._board = [[Color.E] * brdSize for i in range(brdSize)]
        self._lastPlayer = list(Color)[1]
        self._scoreboard = {colorObj: 0 for colorObj in list(Color)[:2]}

        #TODO
        #self.pop()

        #if filename != None:
        #    self._filename = filename
        #    if os.path.isfile(filename):
        #        self._readBoardFromFile(filename)
        #    else:
        #        self._writeBoardToFile(filename)

    def makeMove(self, row, col, color=None):
        #if self._filename == None:
        #    return self._makeMove(row, col, color)
        #else:
        #    self._readBoardFromFile(self._filename)
        #    score, err = self._makeMove(row,col,color)
        #    self._writeBoardToFile(self._filename)
        #    return score, err
        if color == self._lastPlayer:
            return None, False
        return self._makeMove(row, col, None)

    def _makeMove(self, row, col, color=None) -> Tuple[int, bool]:
        if self._board[row][col] != Color.E:
            return None, False

        if color == None:
            color = Color.getOpposite(self._lastPlayer)
            self._lastPlayer = color
            
        placedSoldiersSquad = self._findString(color, row, col)
        enemySquads = self._findAttatchedEnemyStrings(color, row, col)
        enemySquadEliminated = False
        score = 0
        for enemySquad in enemySquads:
            if enemySquad.liberties == {Coord(row, col)}:
                score += self._collectString(enemySquad)
                enemySquadEliminated = True
        
        if (not enemySquadEliminated and
         len(placedSoldiersSquad.liberties) == 0):
         return None, False

        self._board[row][col] = color
        self._scoreboard[self._lastPlayer] += score

        return score, True

    def getScores(self):
        return self._scoreboard

    #TODO
    def pop(self):
        self._board[0][5] = Color.W
        self._board[0][6] = Color.W
        self._board[1][1] = Color.W
        self._board[1][3] = Color.W
        self._board[1][4] = Color.W
        self._board[1][7] = Color.W
        self._board[2][2] = Color.W
        self._board[2][4] = Color.W
        self._board[2][8] = Color.W
        self._board[3][1] = Color.W
        self._board[3][4] = Color.W
        self._board[3][8] = Color.W
        self._board[4][5] = Color.W
        self._board[4][6] = Color.W
        self._board[4][7] = Color.W
        self._board[5][2] = Color.W
        self._board[6][1] = Color.W
        self._board[6][2] = Color.W
        self._board[6][3] = Color.W
        self._board[6][4] = Color.W
        self._board[6][5] = Color.W
        self._board[7][1] = Color.W
        self._board[7][2] = Color.W
        self._board[7][5] = Color.W
        self._board[7][6] = Color.W
        
        
        self._board[1][2] = Color.B
        self._board[1][5] = Color.B
        self._board[1][6] = Color.B
        self._board[2][1] = Color.B
        self._board[2][5] = Color.B
        self._board[2][7] = Color.B
        self._board[3][5] = Color.B
        self._board[3][6] = Color.B
        self._board[3][7] = Color.B
        self._board[4][2] = Color.B
        self._board[5][1] = Color.B
        self._board[5][3] = Color.B
        self._board[5][5] = Color.B
        self._board[6][0] = Color.B
        self._board[6][6] = Color.B
        self._board[7][0] = Color.B
        self._board[7][3] = Color.B
        self._board[7][4] = Color.B
        self._board[7][7] = Color.B
        self._board[8][1] = Color.B
        self._board[8][2] = Color.B
        self._board[8][5] = Color.B
        self._board[8][6] = Color.B
        
    def _findString(self, stringColor, row, col):
        """If targeted at a placed stone, gets its 'squad' info, of color 
        stringColor. Info is NOT ALWAYS based on the CURRENT state of the board -
        it reflects a strings properties should the stringColor be placed
        at row:col. This only matters when finding the string of the stone
        about to be placed - it's included in the string despite not yet being
        placed on the board. 
        """
        liberties = set()
        enemies = set()
        colors = {Coord(row, col)}
        queue = {Coord(row, col)}
        while len(queue) > 0:
            currPos = queue.pop()
            neighbours = self._getNeighbours(currPos.row, currPos.col)
            for neighbour in neighbours:
                coordColor = self._board[neighbour.row][neighbour.col]
                if coordColor == stringColor:
                    if neighbour not in colors:
                        queue.add(neighbour)
                        colors.add(neighbour)
                elif coordColor == Color.E:
                    liberties.add(neighbour)
                else:
                    enemies.add(neighbour)
        
        #row:col will be marked according to the contents of the board,
        #sometimes that means marking a stone about to be placed
        #as a liberty - that breaks the methods contract, never
        #is it advantageous to report the considered root coords as empty
        liberties.discard(Coord(row, col))

        return String(liberties, enemies, colors)

    def _findAttatchedEnemyStrings(self, cellColor, row, col) -> Set:
        neighbours = self._getNeighbours(row, col)
        enemyStrings = set()
        for neighbour in neighbours:
            neighbourColor = self._board[neighbour.row][neighbour.col]
            if (neighbourColor == Color.getOpposite(cellColor) and
                #to avoid duplicate enemy strings in score counting for the elimination 
                all(neighbour not in x.colors for x in enemyStrings)):
                enemyStrings.add(self._findString(neighbourColor,
                 neighbour.row, neighbour.col))
        
        return enemyStrings

    def _collectString(self, string: String):
        points = len(string.colors)
        for coord in string.colors:
            self._board[coord.row][coord.col] = Color.E
        return points

    def _getNeighbours(self, row, col) -> Set:
        neighbours = set()
        #TODO DRY
        if row != 0:#northCheck
            neighbours.add(Coord(row-1, col))
        if row != self._boardSize-1:#southCheck
            neighbours.add(Coord(row+1, col))
        if col != 0:#westCheck
            neighbours.add(Coord(row, col-1))
        if col != self._boardSize-1:#eastCheck
            neighbours.add(Coord(row, col+1))
        
        return neighbours


        