from go import Board, Color
from typing import List, Set
import os


def readBoardFromFile(filename: str) -> Board:
    if not os.path.isfile(filename):
        return None
    with open(filename, 'r') as file:
        #rozmiar planszy
        boardSize = int(file.readline())
        
        #plansza
        board = [[] for x in range(boardSize)]
        for row in range(9):
            line = file.readline()
            for position in line.split():
                board[row].append(Color[position])

        #wynik
        scoreboard = dict()
        for it in range(2):
            scoreWords = file.readline().split()
            scoreboard[Color[scoreWords[0]]] = int(scoreWords[1])

        #gracz o ostatnim ruchu
        lastPlayer = Color[file.readline()] 

        return Board.instantiate(boardSize, board,
            lastPlayer, scoreboard, filename)

def rowToString(listOColors: List[Color]) -> str:
    returnStr = ''
    for cell in listOColors:
        returnStr += str(cell) + " "
    return returnStr

def boardToString(brd: Board) -> str:
    retStr = ''
    for row in brd.getBoard():
        retStr += rowToString(row) + '\n'
    return retStr

def boardToJson(brd: Board) -> Set[str]:
    retDict = dict()
    rowList = [[str(pos) for pos in row] for row in brd.getBoard()]
    retDict['board'] = rowList
    retDict['nextPlayer'] = str(Color.getOpposite(brd.getLastPlayer()))
    #for it, row in enumerate(brd.getBoard()):
    #    retStr.append(rowToString(row))
    return retDict   

def stateToString(brd: Board) -> str:
    def scoreToString(scoreMap):
        returnStr = ''
        for key in scoreMap:
            returnStr += str(key) + " " +str(scoreMap[key]) + "\n"
        return returnStr[:-1]

    returnStr = str(brd.getBoardSize()) + "\n"
    returnStr += boardToString(brd)
    returnStr += scoreToString(brd.getScoreboard()) + "\n"
    returnStr += str(brd.getLastPlayer())
    
    return returnStr

def writeBoardToFile(board: Board, filename: str):
    retStr = stateToString(board)

    with open(filename, 'w') as file:
        file.write(retStr)