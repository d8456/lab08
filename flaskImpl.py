import json
import logging
import sys
from flask import Flask, jsonify, request
from enum import Enum
import os

from go import Board, Color
from helper import boardToJson, readBoardFromFile, writeBoardToFile

class GameType(Enum):
    DISK = 1
    MEMORY = 0

app = Flask(__name__)

gameType = dict()
gameBoard = dict()

@app.route("/")
def hello():
    return "Hello World!"

@app.route("/startgame/disk/<gamename>", methods=['PUT'])
def startGameDisk(gamename: str):
    if gamename not in gameType:
        gameType[gamename] = GameType.DISK
        brd = Board(9)
        if os.path.isfile(gamename):
            brd = readBoardFromFile(gamename)
        writeBoardToFile(brd, gamename)
        logging.debug(gameType)
        return jsonify({'created': True})

    else:
        return jsonify({'alreadyExists': True})

@app.route("/startgame/mem/<gamename>", methods=['PUT'])
def startGameMemory(gamename: str):
    if gamename not in gameType:
        gameType[gamename] = GameType.MEMORY
        gameBoard[gamename] = Board(9)
        return jsonify({'created': True})
    else:
        return jsonify({'alreadyExists': True})
@app.route('/getboard/<gamename>',methods=['GET'])
def getBoard(gamename: str):
    brd = getGame(gamename)
    if brd != None:
        return jsonify(boardToJson(brd))
    else:
        return jsonify({'exists': False})

@app.route('/getscore/<gamename>', methods=['GET'])
def getScores(gamename: str):
    brd = getGame(gamename)
    if brd == None:
        return jsonify({'exists': False})
    scr = brd.getScoreboard()
    return jsonify({ str(list(Color)[0]): scr[list(Color)[0]],
        str(list(Color)[1]): scr[list(Color)[1]]})

@app.route('/makemove/<gamename>',methods=['PUT'])
def makeMove(gamename: str):
    logging.debug(request.json)
    if any(s not in request.json for s
     in ('color', 'row', 'col') or request.json['color'] == None):
        return jsonify({'invalidReqBody': True})
    else:
        color = Color[request.json['color']]
        row = request.json['row']
        col = request.json['col']
    
    brd = getGame(gamename)
    if brd == None:
        return jsonify({'invalidGame': True})
    score, success = brd.makeMove(row, col, color)
    if gameType[gamename] == GameType.DISK:
        writeBoardToFile(brd, gamename)
    if not success:
        return jsonify({'invalidMove': True})
    else:
        brdDict = boardToJson(brd)
        brdDict['score'] = score
        return jsonify(brdDict)

@app.route('/endgame/<gamename>',methods=['DELETE'])
def endGame(gamename: str):
    if gamename not in gameType:
        return jsonify({'exists': False})
    score = getScores(gamename)
    if gameType[gamename] == GameType.DISK:
        os.remove(gamename)
    else:
        gameBoard.pop(gamename)
    gameType.pop(gamename)
    return score

def makeMoveOnGame(brd: Board, row, col, color):
    score, success = brd.MakeMove(row, col, color)


def getGame(gamename: str):
    if gamename in gameType:
        if gameType[gamename] == GameType.DISK:
            return readBoardFromFile(gamename)
        else:
            return gameBoard[gamename]
    else:
        if os.path.isfile(gamename):
            #SRP?
            gameType[gamename] = GameType.DISK
            return readBoardFromFile(gamename)
        else:
            return None

if __name__ == "__main__":
    #if len(sys.argv) == 2:
    #    board = Board(int(sys.argv[1]))
    #elif len(sys.argv) == 3:
    #    board = Board(int(sys.argv[1]), sys.argv[2])
    logging.basicConfig(filename='example.log',
     encoding='utf-8', level=logging.DEBUG)
    app.run()